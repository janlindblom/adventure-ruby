module Adventure
  class World
    attr_reader :rooms
    attr_reader :starting_room
    attr_reader :goal

    def initialize(rooms=nil)
      self.rooms = []
    end

    def enter_room(id=nil, from_room=nil)
      room = self.rooms.find { |r| r.id == id }
      room.enter(from_room) unless room.nil? or from_room.nil?
    end

    private

    def rooms=(rooms)
      @rooms = rooms
    end
  end
end