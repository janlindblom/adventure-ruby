require 'securerandom'

module Adventure
  class Room
    attr_reader :id
    attr_reader :name
    attr_reader :description

    @came_from = nil
    @world = nil

    def initialize(name=nil, description=nil, options=nil)
      @id = SecureRandom.uuid
    end

    def enter(from_room=nil)
      @came_from = from_room
      puts "Entering room #{@id} from #{from_room}"
    end

    def back_out
      @world.enter_room(@came_from)
    end

    def leave(direction)

    end
  end
end