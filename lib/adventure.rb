require "adventure/version"
require "highline"

require "adventure/world"
require "adventure/room"
require "adventure/game"

module Adventure
  def enter_room(description=nil, options=nil)
    room = Adventure::Room.new(description, options)
    room.enter
  end
end
