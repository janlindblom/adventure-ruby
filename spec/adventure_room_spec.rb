RSpec.describe Adventure::Room do
  before(:all) do
    @room = Adventure::Room.new
  end

  it "is a room" do
    expect(@room).to be_a Adventure::Room
  end

  it "can be entered" do
    expect(@room).to respond_to :enter
  end

  it "can be exited" do
    expect(@room).to respond_to :leave
  end
end
