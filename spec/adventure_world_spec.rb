RSpec.describe Adventure::World do
  before(:all) do
    @world = Adventure::World.new
  end

  it "is a world" do
    expect(@world).to be_a Adventure::World
  end

  it "can return a list of rooms" do
    expect(@world).to respond_to :rooms
  end
end
